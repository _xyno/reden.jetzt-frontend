<p align="center">
  <a href="https://reden.jetzt" target="blank"><img src="./reden-jetzt-icon.svg" width="320" alt="Project Logo" /></a>
</p>

# reden.jetzt

Teste reden.jetzt hier: [reden.jetzt](https://www.reden.jetzt)


## Description

Gerade in Zeiten vom Coronavirus sind viele (alleine) zuhause und würden sich gerne unterhalten, um Einsamkeit und Langeweile entgegenzuwirken. Es gibt schon einige Plattformen die einem eine*n Rederpartner*in vermitteln, doch bei den meisten muss man sich entweder anmelden oder der/die Redepartner*in wird komplett per Zufall bestimmt. An dieser Stelle setzt [reden.jetzt](https://reden.jetzt) ein.

Entstanden beim [#WirVsVirusHack](https://wirvsvirushackathon.devpost.com/)

Siehe auch unseren [Devpost](https://devpost.com/software/reden-jetzt)
## Installation

```bash
$ npm install
```

## Development server
```bash
# you only have to run this if you don't have angular cli installed already
$ npm i @angular/cli -g
# you only have to run this, every time you want to regenerate the api-client
$ npm run build
$ npm run start
```

## Build
```bash
# you only have to run this if you don't have angular cli installed already
$ npm i @angular/cli -g
$ npm run build
```

## Running unit tests

```bash
$ npm run test
```
Unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

```bash
$ npm run e2e
```

Tests via [Protractor](http://www.protractortest.org/).


## Documentation

### Routes

 - Home (/)
 - call (/call)
 - specific call (/call/:id)
 - imprint (/imprint)
 - privacy (/privacy)
 - queue (/queue)
   - not yet in use
 - match-found (/match-found)
   - not yet in use

## License 

reden.jetzt is [MIT licensed](LICENSE).

## Code of conduct

Please read our [code of conduct](CODE_OF_CONDUCT.MD) if you want to contribute.
