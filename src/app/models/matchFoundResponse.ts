export interface MatchFoundResponse {
  isCaller: boolean;
  partnerPeerId: string;
  matchedUserTopics: string[];
  ownTopics: string[];
}
