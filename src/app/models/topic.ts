export interface Topic {
    id: string;
    name: string;
    peopleInQueue: number;
    peopleInTotal: number;
    tags: string[];
}
