export interface ChatMessage {
  message: string;
  fromMe: boolean;
}
