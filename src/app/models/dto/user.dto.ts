export class UserDto {
  clientId: string;
  peerId: string;
  topicIds: string[];
  partnerId: string | null;

  name: string;
  minAge: number;
  maxAge: number;
  ownAge: number;

  randomConnect: boolean;
}
