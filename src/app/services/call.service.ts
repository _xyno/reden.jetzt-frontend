import { async } from '@angular/core/testing';
import { TopicService } from './topic.service';
import { ChatMessage } from './../models/chatMessage';
import { environment } from './../../environments/environment';
import { Injectable, OnInit } from '@angular/core';
import Peer, { DataConnection, MediaConnection } from 'peerjs';
import { Topic } from '../models/topic';
import {Person} from '../models/person';

@Injectable({
  providedIn: 'root'
})
export class CallService {
  private myPeer: Peer = null;
  private chatConnection: Peer.DataConnection = null;
  public messages: ChatMessage[] = [];
  public recv: (data: any) => any;
  public outgoingGain: GainNode = null;
  public incomingGain: GainNode = null;
  public matchFound = false;
  public matchingTopics: Topic[] = [];
  public ownTopics: Topic[] = [];
  public participants: Person[];

  public audioCallback: (gain: number) => void;
  public closeConnectionCallback: () => any;

  public ownPeerID() {
    return this.myPeer.id;
  }

  constructor(
    private topicService: TopicService
  ) {}

  public setRecvHandler(recv: (data: any) => any) {
    this.recv = recv;
  }

  public initPeer() {
    if (this.myPeer === null) {
      this.myPeer = new Peer(environment.peerConfig);
    }
    if (this.myPeer.disconnected) {
      this.myPeer = new Peer(environment.peerConfig);
    }
  }

  public setOnReceive() {
    this.myPeer.on('connection', data => {
      this.chatConnection = data;
      this.chatReceiveListner();
    });
  }

  public registerOnCall(cb: (mediaStream: MediaStream) => void, cbClose: () => any) {
    this.myPeer.on('call', async mediaConnection => {
      this.matchFound = true;
      mediaConnection.answer(await this.getAudio());
      mediaConnection.on('stream', (stream) => {
        cb(stream);
      });
      mediaConnection.on('close', () => {
        this.closeConnectionCallback();
      });
    });
  }

  public registerOnError(cb: (err: any) => void) {
    this.myPeer.on('error', (err: any) => {
      console.error(err);
      cb(err);
    });
  }

  public registerOnOpen(cb: () => void) {
    this.myPeer.on('open', () => {
      console.log('Connection opened');
      cb();
    });
  }

  public registerOnClose(cb: () => void) {
    this.myPeer.on('disconnected', () => {
      console.log('disconnected');
      cb();
    });
  }

  public registerOnDisconnect(cb: () => void) {
    this.myPeer.on('close', () => {
      console.log('close');
      cb();
    });
  }

  public connect(peerID: string) {
    this.chatConnection = this.myPeer.connect(peerID);
    this.chatReceiveListner();
  }

  public async call(peerID: string): Promise<MediaConnection> {
    this.matchFound = true;
    const myCall = this.myPeer.call(peerID, await this.getAudio());
    myCall.on('close', () => {
      this.closeConnectionCallback();
    });

    return myCall;
  }

  public sendMessage(text: string) {
    this.messages.push({ message: text, fromMe: true });
    this.chatConnection.send(text);
  }

  private chatReceiveListner() {
    this.chatConnection.on('data', (data: any) => {
      this.messages.push({ message: data, fromMe: false });
      this.recv(data);
    });
  }

  public async getAudio(): Promise<MediaStream> {
    return await navigator.mediaDevices.getUserMedia({
      audio: true,
      video: false
    });
  }

  public setIncomingStream(stream): MediaStream {
    const streamAndGain = this.toGainStream(stream);
    this.incomingGain = streamAndGain.gain;
    return streamAndGain.stream;
  }

  public async getOutgoingStream(): Promise<MediaStream> {
    const streamAndGain = this.toGainStream(await this.getAudio());
    this.outgoingGain = streamAndGain.gain;
    return streamAndGain.stream;
  }

  public toGainStream(stream: MediaStream): StreamAndGain {
    const mediaStream = stream;
    const audioContext = new AudioContext();
    const sourceStream = audioContext.createMediaStreamSource(mediaStream);
    const gain = audioContext.createGain();
    sourceStream.connect(gain);
    gain.connect(audioContext.destination);
    return {
      stream: audioContext.createMediaStreamDestination().stream,
      gain
    };
  }

  public muteAudio() {
    this.outgoingGain.gain.value = 0;
  }

  public unmuteAudio() {
    this.outgoingGain.gain.value = 1;
  }

  public setMatchingTopics(uuids: Topic[]) {
    this.matchingTopics = uuids;
  }

  public setParticipants(participants: Person[]) {
    this.participants = participants;
  }

  public getParticipants(): Person[] {
    return this.participants;
  }
}

interface StreamAndGain {
  stream: MediaStream;
  gain: GainNode;
}
