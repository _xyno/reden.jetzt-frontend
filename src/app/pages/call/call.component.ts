import { Options } from 'ng5-slider';
import { CallService } from './../../services/call.service';
import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import {Person} from '../../models/person';

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.scss']
})
export class CallComponent implements OnInit {

  public audioValue = 100;
  public options: Options = {
    floor: 0,
    ceil: 100
  };

  public callPeerID = '';

  constructor(public callService: CallService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.callService.initPeer();
    const callID: string = this.route.snapshot.paramMap.get('id');
    if (callID !== null) {
      this.callPeerID = callID;
    }
  }

  public sliderChanged(): void {
    if (this.callService.audioCallback !== null) {
      this.callService.audioCallback(this.audioValue / 100);
    }
  }

  public endCall(): void {
    const { location } = window;
    location.replace(location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : ''));
  }

  // TOOD: hook this function up to disconnect event
  public onHangup(): void {
    Swal.fire({
      allowOutsideClick: false,
      allowEscapeKey: false,
      icon: 'warning',
      title: 'Dein Gesprächspartner hat aufgelegt.',
      html: 'Geh zurück auf die Startseite und finde einen neuen Gesprächspartner'
    }).then(() => this.endCall());
  }


  public setMute(microOn: boolean) {
    if (microOn) {
      this.callService.unmuteAudio();
    } else {
      this.callService.muteAudio();
    }
  }

  public openHelp() {
    Swal.fire({
            icon: 'question',
            title: 'Hilfe',
            customClass: {
              content: 'call-help-modal'
            },
            html:
          `<p><b>Der Anruf funktioniert nicht?</b> Prüfe bitte folgendes:</p>
            <ol class="text-left">
            <li>
            Du musst in deinem Browser die Verwendung des Mikrofons erlauben. In den meisten Browsern findest du oben in der
            Browserleiste ein Icon auf das du klicken kannst, um das Mikrofon anzuschalten</li>
            <li>
               Manche Browser blockieren Autoplay, dadurch wird der Ton deines Gesprächspartners nicht abgespielt. Du kannst deine
               Einstellungen meist ebenfalls durch ein Icon in der Browserleiste anpassen.
            </li>
            <li>
                Ihr habt Verbindungsprobleme. Bitte verlasse den Anruf und versuche dich erneut zu verbinden.
            </li>
            <li>
            Solltest du das Problem nicht gelöst bekommen, schreibe uns doch bitte eine E-Mail mit allen Details an
            <a href="mailto:wir-muessen@reden.jetzt">wir-muessen@reden.jetzt</a>. Dann versuchen wir dir weiterzuhelfen!
            </li>
            </ol>`
          });
    }

  public getParticipants(): Person[] {
    return this.callService.getParticipants();
  }

}
