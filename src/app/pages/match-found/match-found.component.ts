import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-match-found',
  templateUrl: './match-found.component.html',
  styleUrls: ['./match-found.component.scss']
})
export class MatchFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
