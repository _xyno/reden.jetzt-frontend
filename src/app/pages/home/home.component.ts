import { TopicService } from './../../services/topic.service';
import { async } from '@angular/core/testing';
import { CallService } from './../../services/call.service';
import { AgeSelectorComponent } from './../../components/age-selector/age-selector.component';
import { MatchesService } from './../../services/matches.service';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Topic } from 'src/app/models/topic';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public selectedTopics: Topic[] = null;
  @ViewChild('age')
  public ageSelector: AgeSelectorComponent;
  public connections = 0;

  constructor(
    private router: Router,
    private matchesService: MatchesService,
    private callService: CallService,
    private changeDetectorRef: ChangeDetectorRef,
    private topicService: TopicService
  ) {}

  ngOnInit(): void {
    this.callService.initPeer();
    this.matchesService.OnTotalConnections(connectionNumber => {
      this.connections = connectionNumber;
      this.changeDetectorRef.markForCheck();
    });
  }

  public changeSelectedTopicsOutput(topics: Topic[]) {
    this.selectedTopics = topics;
  }

  public async testAudio(): Promise<any> {
    return this.callService.getAudio();
  }

  public startSearch() {
    if (this.selectedTopics == null) {
      Swal.fire({
        allowOutsideClick: false,
        allowEscapeKey: false,
        icon: 'warning',
        title: 'Bitte wähle mindestens ein Gesprächsthema aus!'
      });
    } else if (isNaN(this.ageSelector.ageValue) && this.ageSelector.ageValue > 0 && this.ageSelector.ageValue < 150) {
      Swal.fire({
        allowOutsideClick: false,
        allowEscapeKey: false,
        icon: 'warning',
        title: 'Bitte gebe dein Alter an!'
      });
    } else {
      Swal.fire({
        allowOutsideClick: false,
        allowEscapeKey: false,
        icon: 'warning',
        title: 'Achtung',
        html:
          'Auf reden.jetzt herrscht ein respektvoller Umgang! Diskriminierungen o.ä sind inakzeptal! Der Missbrauch für nicht-jugendfreie Zwecke ist nicht gestattet!'
      }).then(() => {
        this.matchesService.signUp({
          maxAge:
            this.ageSelector.ageValue + this.ageSelector.ageRangeValue,
          minAge:
            this.ageSelector.ageValue - this.ageSelector.ageRangeValue,
          ownAge: this.ageSelector.ageValue,
          peerId: this.callService.ownPeerID(),
          topicIds: this.selectedTopics.map<string>(topic => {
            return topic.id;
          })
        });

        Swal.fire({
          allowOutsideClick: false,
          allowEscapeKey: false,
          title: 'Bitte aktiviere deine Mikrofon!',
          html: 'Bitte gebe dein Mikrofon in deinem Browser frei. Weitere Details findest du in der Hilfe.',
          timerProgressBar: true,
          footer:
            '<button class="btn btn-danger" onClick="window.location.reload();">Seite neuladen</button>',
          onBeforeOpen: () => {
            Swal.showLoading();
            this.testAudio().then(() => {
              Swal.close();
              this.initCall();
            }).catch(() => {
              Swal.close();
              Swal.fire({
                icon: 'error',
                html: 'Das Mikrofon muss freigeschaltet werden',
                footer:
                'Sollte es nicht funktioniert haben, lade die Seite neu!',
                showConfirmButton: false
              }
              );
            });
          },
        });
      });
    }
  }

  private async initCall() {
    this.matchesService.OnMatchFound(async (data: any) => {
      console.log(data);
      this.callService.setParticipants(
          [
              {
                name: '',
                topics: await this.topicService.uuidsToTopics(data.ownTopics)
              },
              {
                  name: '',
                  topics: await this.topicService.uuidsToTopics(data.matchedUserTopics)
              }
          ]
      );
      console.log('Match found!');
      if (data.isCaller) {
        this.router.navigateByUrl(`/call/${data.partnerPeerId}`);
      } else {
        this.router.navigateByUrl('/call');
      }
      Swal.close();
    });
    console.log('start search!');
    this.matchesService.findMatch();
    new Promise(r => setTimeout(r, 60000)).then(() => {
      console.log('random!');
      if (!this.callService.matchFound) {
        this.matchesService.randomConnect();
        Swal.close();
        Swal.fire({
          title: 'Noch kurz Geduld!',
          html: 'Es wird jetzt ein zufälliger Gesprächspartner gesucht.',
          timerProgressBar: true,
          showConfirmButton: true,
          footer: '<button class="btn btn-danger" onClick="window.location.reload();"> Suche abbrechen! </button>',
          onBeforeOpen: () => {
            Swal.showLoading();
          }
        });
        this.matchesService.findMatch();
      }
    });
    Swal.close();
    Swal.fire({
      allowOutsideClick: false,
      allowEscapeKey: false,
      title: 'Noch kurz Geduld!',
      html: 'Es wird ein Gesprächspartner gesucht.',
      timerProgressBar: true,
      showConfirmButton: true,
      footer: '<button class="btn btn-danger" onClick="window.location.reload();"> Suche abbrechen! </button>',
      onBeforeOpen: () => {
        Swal.showLoading();
      }
    });
  }
}
