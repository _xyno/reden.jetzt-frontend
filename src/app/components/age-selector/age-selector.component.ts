import { Component, ElementRef, ViewChild, Output, QueryList, ViewChildren } from '@angular/core';
import { Options } from 'ng5-slider';

@Component({
    selector: 'app-age-selector',
    templateUrl: './age-selector.component.html',
    styleUrls: ['./age-selector.component.scss']
})
export class AgeSelectorComponent {
  get ageValue() {
    return parseInt((document.getElementById('age') as any ).value, -1);
  }

  ageRangeValue = 5;
  ageRangeOptions: Options = {
    floor: 0,
    ceil: 50
  };
}
