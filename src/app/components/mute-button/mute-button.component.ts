import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mute-button',
  templateUrl: './mute-button.component.html',
  styleUrls: ['./mute-button.component.scss']
})
export class MuteButtonComponent implements OnInit {

  @Output() changedMute: EventEmitter<boolean> = new EventEmitter();

  toggle = true;
  status = 'Enable';

  toggleRule() {
      this.toggle = !this.toggle; // true mikro an
      this.status = this.toggle ? 'Enable' : 'Disable';
      this.changedMute.emit(this.toggle);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
