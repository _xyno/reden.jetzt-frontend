import { environment } from './../../../environments/environment';
import { ChatMessage } from './../../models/chatMessage';
import { CallService } from './../../services/call.service';
import { Component, OnInit, ChangeDetectorRef, Input, AfterViewInit, ViewChild, ElementRef, QueryList, ViewChildren } from '@angular/core';
import Swal from 'sweetalert2';
import { VirtualTimeScheduler } from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  @Input() callPeerID: string;
  public audioVolume: number;
  public workingAudio = false;

  public getMessages(): ChatMessage[] {
    return this.callService.messages;
  }

  constructor(private callService: CallService,
              private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.callService.registerOnError((err) => {
      this.showErr();
    });
    this.callService.registerOnClose(() => {
      Swal.fire( {
        allowOutsideClick: false,
        allowEscapeKey: false,
        icon: 'error',
        title: 'Verbindung abgebrochen!',
        html: 'Das Telefonat wurde beendet',
        confirmButtonText: 'Zurück zur Startseite!'
       }).then(() => {
        this.moveToHome();
       });
    });

    this.callService.closeConnectionCallback = () => {
      Swal.fire( {
        allowOutsideClick: false,
        allowEscapeKey: false,
        icon: 'error',
        title: 'Verbindung abgebrochen!',
        html: 'Das Telefonat wurde beendet',
        confirmButtonText: 'Zurück zur Startseite!'
       }).then(() => {
        this.moveToHome();
       });
    };

    this.callService.setOnReceive();

    this.callService.registerOnCall(async (mediaStream) => {
      this.playAudio(mediaStream);
    }, () => {
      Swal.fire( {
        allowOutsideClick: false,
        allowEscapeKey: false,
        icon: 'error',
        title: 'Verbindung abgebrochen!',
        html: 'Das Telefonat wurde beendet',
        confirmButtonText: 'Zurück zur Startseite!'
       }).then(() => {
        this.moveToHome();
       });
    });

    this.callService.setRecvHandler((data: any) => {
      console.log(`received chat ${data}`);
      console.log(this.callService.messages);
      this.cd.detectChanges();
    });

    new Promise( resolve => setTimeout(resolve, 1000)).then( () => {
      if (this.callPeerID !== '') {
        this.connect(this.callPeerID);
      }
    });

    this.callService.audioCallback = (volume: number) => {
      this.audioVolume = 0;
      this.cd.detectChanges();
    };
    this.timeout();
  }

  public onEnter(content: string) {
    (document.getElementById('message') as any).value = '';
    this.callService.sendMessage(content.length > 1000 ? content.substr(0, 1000) : content);
  }

  public getOwnID() {
    return this.callService.ownPeerID();
  }

  public async connect(toConnectTo: string) {
    this.callService.connect(toConnectTo);
    const call = await this.callService.call(toConnectTo);
    call.on('stream', (stream) => {
      this.playAudio(stream);
    });
  }

  public playAudio(stream: MediaStream) {
    this.workingAudio = true;
    const audio: any = document.getElementById('audio-play');
    audio.srcObject = stream;
    audio.play();
  }

  public muteAudio() {
    this.callService.muteAudio();
  }

  public moveToHome() {
    window.location.href = '/';
  }

  public timeout() {
    new Promise(r => setTimeout(r, environment.calltimeout)).then(() => {
      if (!this.workingAudio) {
        this.showErr();
      }
    });
  }

  public showErr() {
    Swal.fire( {
      allowOutsideClick: false,
      allowEscapeKey: false,
      icon: 'error',
      title: 'Fehler!',
      html: 'Es ist ein Fehler aufgetreten',
      confirmButtonText: 'Zurück zur Startseite!'
     }).then(() => {
      this.moveToHome();
     });
  }

}
