import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Topic} from '../../models/topic';

@Component({
    selector: 'app-topic-selector-button',
    templateUrl: './topic-selector-button.component.html',
    styleUrls: ['./topic-selector-button.component.scss']
})
export class TopicSelectorButtonComponent implements OnInit {
    @Input() public topic: Topic;
    @Output() public selectionStateChanged: EventEmitter<Topic> = new EventEmitter<Topic>();
    @Input() public selected: boolean;

    constructor() {
    }

    changeState(): void {
        if (this.topic !== null) {
            this.selectionStateChanged.emit(this.topic);
        }
    }

    ngOnInit(): void {
    }

}
