import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicSelectorButtonComponent } from './topic-selector-button.component';

describe('TopicSelectorButtonComponent', () => {
  let component: TopicSelectorButtonComponent;
  let fixture: ComponentFixture<TopicSelectorButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopicSelectorButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicSelectorButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
