import {Component, Input, OnInit} from '@angular/core';
import {Person} from '../../models/person';

@Component({
    selector: 'app-partner-information',
    templateUrl: './partner-information.component.html',
    styleUrls: ['./partner-information.component.scss']
})
export class PartnerInformationComponent implements OnInit {
    @Input() partner: Person = {
        name: 'Name not found',
        topics: []
    };

    constructor() {
    }

    ngOnInit(): void {
    }

}
