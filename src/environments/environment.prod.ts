export const environment = {
  production: false,
  socketIOUrl: 'https://api.reden.jetzt/matches',
  apiBasePath: 'https://api.reden.jetzt',
  peerConfig: {
    host: 'ich.lasse.deine.peerjs.clients.reden.jetzt',
    path: '/myapp',
    secure: true
  },
  calltimeout: 10000
};
